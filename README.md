# Pipeline status

[![pipeline status](https://gitlab.com/Sparxx/automatic-changelog/badges/master/pipeline.svg)](https://gitlab.com/Sparxx/automatic-changelog/-/commits/master)

# Description
The .autochanglog.yml generates a daylie git log report in to CHANGELOG.md. Only changes from yesterday will be commitet every night. A job schedule will export the log to CHANGELOG.md every night. The .autochanglog.yml file is project and user neutral and will work in every other project after installation.

# Instllation
## Preparations
First, an SSH key pair must be created. The following command is necessary for this.  
<code>ssh-keygen -f ~/filename -t rsa -b 4096</code>

Now the public key can be added to our gitlab project. The option for this can be found under "Settings/Repository/Deploy keys".  
Here "Deploy keys" must be extended
Now the public key can be added with a name of your choice.

Afterwards a CI/CD variable must be created. This variable contains the private part of the key.
This option can be found under "Settings/CI/CD/Variables". Here please also extend Variables again and create a new variable with the name "SSH_PRIVATE" and insert the private part of the SSH key as content.

This concludes the preparations and the actual part of the installation begins.

## Main part
Upload the file .autochangelog.yml

Adding the job to .gitlab-ci.yml
<code>  
create-changelog:
    stage: CHANGELOG
    only:
        - schedules
    trigger:
        include:
            - local: '.autochangelog.yml'.
</code>

Subsequently, the CI/CD schduled taks must still be created.
This item can be found under "CI/CD/Schedules".
Only a job must be created that runs at any time once a day. The name does not matter.

finish
